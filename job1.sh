nvcc --version
python --version
whereis cudnn.h
pip -V

pip install --user matplotlib
pip install --user setproctitle
pip install --user mpi4py
pip install --user gym
cd ~
git clone https://DennisWangWC@bitbucket.org/DennisWangWC/gym-uav.git
cd gym-uav
pip install --user -e .
cd ..
git clone https://DennisWangWC@bitbucket.org/DennisWangWC/ddpgfd-master.git
cd ddpgfd-master
cd baselines
pip install --user -e .
python -m baselines.run --alg=ddpg --env=uav-v0 --num_epochs=15000 --seed=1 --expert_path=//philly/gcr/resrchvc/v-wancha/demonstrations/uav_0_0.npz --variance=0.0 --traj_limitation=1 --cluster=gcr